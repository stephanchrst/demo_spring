DROP TABLE IF EXISTS booking;

CREATE TABLE booking (
  id serial PRIMARY KEY,
  booking_name VARCHAR(250)
);
  
insert into booking(booking_name) values('yosia');