package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@ComponentScan
public class BookingCommandLineRunner implements CommandLineRunner{

	@Override
	public void run(String... args) throws Exception {
		
		for (Booking b : this.bookingRepo.findAll()) {
			System.out.println(b.toString());
		}
		
	}

	@Autowired BookingRepository bookingRepo;
}
