package com.example.demo;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

public class BookingRestController {

	@RequestMapping("/bookings")
	Collection<Booking> bookings(){
		return this.bookingRepo.findAll();
	}

	@Autowired BookingRepository bookingRepo;
}
